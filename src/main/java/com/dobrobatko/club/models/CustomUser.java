package com.dobrobatko.club.models;

import com.dobrobatko.club.dto.CustomUserDTO;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "Members")
public class CustomUser {
    @Id
    @GeneratedValue
    private Long id;
    private String login;
    private String password;
    @Enumerated(EnumType.STRING)
    private UserRole role;
    private String email;
    @Column(length = 1024)
    private String bio;
    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(name = "user_country",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "country_id"))
    private Set<Country> countries = new HashSet<>();
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Message> messages = new ArrayList<>();
    @Column(nullable = false)
    private Integer msgCount;

    public CustomUser() {
    }

    public CustomUser(String login, String password, UserRole role,
                      String email, String bio) {
        this.login = login;
        this.password = password;
        this.role = role;
        this.email = email;
        this.bio = bio;
        this.msgCount = 0;
    }

    public CustomUser(String login, String password, UserRole role,
                      String email, String bio, List<Message> messages) {
        this.login = login;
        this.password = password;
        this.role = role;
        this.email = email;
        this.bio = bio;
        this.messages = messages;
        this.msgCount = 0;
    }

    public void addMessage (Message message) {
        messages.add(message);
        message.setUser(this);
    }

    public void addCountry(Country country) {
        countries.add(country);
        country.getCustomUsers().add(this);
    }

    public void clearCountries() {
        countries.clear();
    }

    public CustomUserDTO toDTO() {
        Set<Long> countriesId = new HashSet<>();
        Set<String> countriesName = new HashSet<>();
        countries.forEach((c) -> {
            countriesId.add(c.getId());
            countriesName.add(c.getName());
        });
        return CustomUserDTO.of(id, login, email, bio, countriesId, countriesName);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public Set<Country> getCountries() {
        return countries;
    }

    public void setCountries(Set<Country> countries) {
        this.countries = countries;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public Integer getMsgCount() {
        return msgCount;
    }

    public void setMsgCount(Integer msgCount) {
        this.msgCount = msgCount;
    }
}
