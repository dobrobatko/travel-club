package com.dobrobatko.club.models;

import com.dobrobatko.club.dto.MessageDTO;
import javax.persistence.*;
import java.util.Date;

@Entity
public class Message {

    @Id
    @GeneratedValue
    private Long id;
    private Long fromUserId;
    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp;
    private String subject;
    private String body;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private CustomUser user;

    public Message() {
    }

    public Message(Long fromUserId, Date timestamp, String subject, String body, CustomUser user) {
        this.fromUserId = fromUserId;
        this.timestamp = timestamp;
        this.subject = subject;
        this.body = body;
        this.user = user;
    }

    public MessageDTO toDTO() {
        return MessageDTO.of(id, fromUserId, timestamp, subject, body);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(Long fromUserId) {
        this.fromUserId = fromUserId;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public CustomUser getUser() {
        return user;
    }

    public void setUser(CustomUser user) {
        this.user = user;
    }
}
