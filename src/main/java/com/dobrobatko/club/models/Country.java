package com.dobrobatko.club.models;

import com.dobrobatko.club.dto.CountryDTO;
import javax.persistence.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Entity
public class Country {
    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false)
    private String name;
    @Column(length = 9999)
    private String description;
    private String imageLink;
    @ManyToMany(mappedBy = "countries")
    private Set<CustomUser> customUsers = new HashSet<>();


    public Country() {
    }

    public Country(String name, String description, String imageLink) {
        this.name = name;
        this.description = description;
        this.imageLink = imageLink;
    }

    public Country(Long id, String name, String description, String imageLink) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.imageLink = imageLink;
    }

    public static Country of(Long id, String name, String description, String imageLink) {
        return new Country(id, name, description, imageLink);
    }

    public void addUser(CustomUser user) {
        customUsers.add(user);
        user.getCountries().add(this);
    }

    public CountryDTO toDTO() {
        Map<Long, String> users = new HashMap<>();
        customUsers.forEach(c -> {
            users.put(c.getId(), c.getLogin());
        });
        return CountryDTO.of(id, name, description, imageLink, users);
    }

    public static Country fromDTO(CountryDTO countryDTO) {
        return Country.of(countryDTO.getId(), countryDTO.getName(), countryDTO.getDescription(), countryDTO.getImageLink());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public Set<CustomUser> getCustomUsers() {
        return customUsers;
    }

    public void setCustomUsers(Set<CustomUser> customUsers) {
        this.customUsers = customUsers;
    }

    @Override
    public String toString() {
        return "Country{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", imageLink='" + imageLink + '}';
    }
}
