package com.dobrobatko.club.models;

import com.dobrobatko.club.dto.NewsDTO;
import javax.persistence.*;
import java.util.Date;

@Entity
public class News {

    @Id
    @GeneratedValue
    private Long id;
    @Temporal(TemporalType.DATE)
    @Column(nullable = false)
    private Date date;
    @Column(nullable = false)
    private String header;
    @Column(length = 9999)
    private String body;

    public News(){}

    public News(Date date, String header, String body) {
        this.date = date;
        this.header = header;
        this.body = body;
    }

    public NewsDTO toDTO () {
        return NewsDTO.of(date, header,body);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
