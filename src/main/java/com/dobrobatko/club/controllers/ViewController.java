package com.dobrobatko.club.controllers;

import com.dobrobatko.club.dto.LocationDTO;
import com.dobrobatko.club.maintenance.EmailValidator;
import com.dobrobatko.club.maintenance.UsernameValidator;
import com.dobrobatko.club.models.UserRole;
import com.dobrobatko.club.retrievers.GeoRetriever;
import com.dobrobatko.club.services.LocationService;
import com.dobrobatko.club.services.NewsService;
import com.dobrobatko.club.services.UserService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class ViewController {

    private final UserService userService;
    private final PasswordEncoder passwordEncoder;
    private final UsernameValidator usernameValidator;
    private final EmailValidator emailValidator;
    private final NewsService newsService;
    private final LocationService locationService;
    private final GeoRetriever geoRetriever;


    public ViewController(UserService userService, PasswordEncoder passwordEncoder,
                          EmailValidator emailValidator, UsernameValidator usernameValidator, NewsService newsService,
                          LocationService locationService, GeoRetriever geoRetriever) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
        this.usernameValidator = usernameValidator;
        this.emailValidator = emailValidator;
        this.newsService = newsService;
        this.locationService = locationService;
        this.geoRetriever = geoRetriever;
    }

    @GetMapping("/")
    public String onIndex() {
        return "index";
    }

    @GetMapping("/register")
    public String registerPage() {
        return "register";
    }

    @GetMapping("/login")
    public String loginPage(HttpServletRequest request, Model model,
                            @RequestParam(required = false, defaultValue = "") String error,
                            @RequestParam(required = false, defaultValue = "") String registered) {
        if (error.equals("") && registered.equals("")) {
            String ip = request.getRemoteAddr();
            LocationDTO location = geoRetriever.getLocation(ip);
            locationService.save(location);
        }
        String msg = "";
        String result = "";
        if (error.equals("wrong")) {
            msg = "Wrong login or password! Please, try again.";
        }
        if (registered.equals("new")) {
            result = "New user registered successfully! You can login in.";
        }
        model.addAttribute("msg", msg);
        model.addAttribute("result", result);
        return "login";
    }

    @GetMapping("/out")
    public String logoutPage() {
        return "out";
    }


    @GetMapping("/unauthorized")
    public String unauthorizedPage() {
        return "unauthorized";
    }

    @PostMapping("/newmember")
    public String newMember(Model model,
                            @RequestParam String n_login,
                            @RequestParam String n_password,
                            @RequestParam String n_confirm,
                            @RequestParam String n_email) {
        String passHash = passwordEncoder.encode(n_password);

        if (!usernameValidator.validate(n_login)) {
            model.addAttribute("msg", "Username is too long! Please, try shorter username.");
            return "register";
        }

        if (!n_password.equals(n_confirm)) {
            model.addAttribute("msg", "The specified passwords do not match!");
            return "register";
        }

        if (!emailValidator.validate(n_email)) {
            model.addAttribute("msg", "Incorrect e-mail address! Please, try again.");
            return "register";
        }

        if (!userService.addUser(n_login, passHash, UserRole.USER, n_email, "")) {
            model.addAttribute("msg", "Member with the same login already exists! Please, try again.");
            return "register";
        }
        return "redirect:/login?registered=new";
    }

    @GetMapping("/admin")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String adminPage(Model model) {
        model.addAttribute("news", newsService.getAllNews());
        model.addAttribute("users", userService.getAllUsers());
        return "admin";
    }

    @PostMapping("/deletenews")
    public String deleteSelectedNews(@RequestParam(name = "newsToDelete[]", required = false) List<Long> ids,
                                     Model model) {
        newsService.deleteNews(ids);
        model.addAttribute("news", newsService.getAllNews());
        return "admin";
    }

    @PostMapping("/addnews")
    public String addNews(@RequestParam(required = true) String header,
                          @RequestParam(required = true) String body) {
        newsService.addNews(header, body);
        return "redirect:/admin";
    }

    @PostMapping("/deleteusers")
    public String deleteUsers(@RequestParam(name = "usersToDelete[]", required = false) List<Long> ids,
                              Model model) {
        userService.deleteUsers(ids);
        model.addAttribute("users", userService.getAllUsers());
        return "admin";
    }

}


