package com.dobrobatko.club.controllers;

import com.dobrobatko.club.dto.CountryDTO;
import com.dobrobatko.club.dto.CustomUserDTO;
import com.dobrobatko.club.dto.MessageDTO;
import com.dobrobatko.club.dto.NewsDTO;
import com.dobrobatko.club.models.Country;
import com.dobrobatko.club.models.CustomUser;
import com.dobrobatko.club.services.CountryService;
import com.dobrobatko.club.services.MessageService;
import com.dobrobatko.club.services.NewsService;
import com.dobrobatko.club.services.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;
import java.util.*;

@RestController
public class APIController {

    private final CountryService countryService;
    private final NewsService newsService;
    private final UserService userService;
    private final MessageService messageService;

    public APIController(CountryService countryService, NewsService newsService, UserService userService, MessageService messageService) {
        this.countryService = countryService;
        this.newsService = newsService;
        this.userService = userService;
        this.messageService = messageService;
    }

    // User APIs
    @GetMapping("api/getcurrentusername")
    public String getCurrentUserName() {
        User user = getCurrentUser();
        return user.getUsername();
    }

    @GetMapping("api/getcurrentuser")
    public CustomUserDTO getProfileOfCurrentUser() {
        User user = getCurrentUser();
        String login = user.getUsername();
        CustomUser cuser = userService.findByLogin(login);
        return cuser.toDTO();
    }

    @GetMapping("api/user")
    public CustomUserDTO getUserById(@RequestParam(required = true) String id) {
        return userService.getUserById(Long.valueOf(id));
    }

    @PostMapping(value = "api/updateprofile")
    public ResponseEntity<Void> updateProfile(@RequestBody Map<String, Object> payLoad) {
        Long userId = Long.valueOf((Integer) payLoad.get("user"));
        String userLogin = (String) payLoad.get("login");
        String email = (String) payLoad.get("email");
        String bio = (String) payLoad.get("bio");
        List<Integer> countriesID = (List<Integer>) payLoad.get("countries");

        Set<Country> countries = new HashSet<>();
        for (Integer id : countriesID) {
            countries.add(Country.fromDTO(countryService.getCountryById(Long.valueOf(id))));
        }
        userService.updateUser(userId, userLogin, email, bio, countries);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    public User getCurrentUser() {
        return (User) SecurityContextHolder
                .getContext().getAuthentication()
                .getPrincipal();
    }

    //Country APIs
    @GetMapping("api/getcountries")
    public List<CountryDTO> getCountries() {
        return countryService.getAllCountries();
    }

    @GetMapping("api/getcountriesnames")
    public List<CountryDTO> getCountriesNames() {
        return countryService.getAllNames();
    }

    @GetMapping("api/getcountry")
    public CountryDTO getCountry(@RequestParam(required = true) String id) {
        return countryService.getCountryById(id);
    }

    //News API
    @GetMapping("api/getnewspreviews")
    public List<NewsDTO> getNewsPreviews() {
        return newsService.createNewsPreviews();
    }

    @GetMapping("api/getnews")
    public NewsDTO getNews(@RequestParam(required = true) String id) {
        return newsService.getNewsByID(id);
    }

    //Message API
    @PostMapping(value = "api/sendmessage")
    public ResponseEntity<Void> sendMessage(@RequestBody Map<String, Object> payLoad) {
        Long toUserId = Long.valueOf((Integer) payLoad.get("toUserId"));
        Long fromUserId = Long.valueOf((Integer) payLoad.get("fromUserId"));
        String subject = (String) payLoad.get("subject");
        String text = (String) payLoad.get("text");

        if (userService.getUserById(toUserId) == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        userService.addMessage(toUserId, fromUserId, subject, text);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("api/messagecount")
    public Integer newMsgCount(@RequestParam String id) {
        return messageService.howManyNew(Long.valueOf(id));
    }

    @GetMapping("api/getmessages")
    public List<MessageDTO> getMessages(@RequestParam String id) {
        return messageService.getAllMessages(Long.valueOf(id));
    }

    @GetMapping("api/resetcounter")
    public ResponseEntity<Void> resetCounterOfNewMsgs(@RequestParam String id) {
        messageService.resetCounter(Long.valueOf(id));
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("api/deletemessage")
    public ResponseEntity<Void> deleteMessage(@RequestParam String id) {
        messageService.deleteMessage(Long.valueOf(id));
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
