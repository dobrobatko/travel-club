package com.dobrobatko.club.services;

import com.dobrobatko.club.dto.CountryDTO;
import com.dobrobatko.club.models.Country;
import com.dobrobatko.club.repositories.CountryRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.ArrayList;
import java.util.List;
import static java.lang.Long.parseLong;

@Service
public class CountryService {

    private final CountryRepository countryRepository;

    public CountryService(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    @Transactional
    public boolean addCountry (String name, String description, String imageLink) {
        if(countryRepository.existsByName(name)) {
            return false;
        }
        Country country = new Country(name, description, imageLink);
        countryRepository.save(country);
        return true;
    }

    @Transactional(readOnly = true)
    public List<CountryDTO> getAllCountries() {
        List<CountryDTO> result = new ArrayList<>();
        List<Country> countries = countryRepository.findAll();
        countries.forEach((c) -> result.add(c.toDTO()));
        return result;
    }

    @Transactional(readOnly = true)
    public List<CountryDTO> getAllNames() {
        return  countryRepository.findAllNames();
    }

    @Transactional(readOnly = true)
    public CountryDTO getCountryById(String id) {
        Country country = countryRepository.findById(parseLong(id)).get();
        return country.toDTO();
    }

    @Transactional(readOnly = true)
    public CountryDTO getCountryById(Long id) {
        Country country = countryRepository.findById(id).get();
        return country.toDTO();
    }
}
