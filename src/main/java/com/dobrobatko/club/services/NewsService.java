package com.dobrobatko.club.services;

import com.dobrobatko.club.dto.NewsDTO;
import com.dobrobatko.club.maintenance.NewsDTOcomparator;
import com.dobrobatko.club.models.News;
import com.dobrobatko.club.repositories.NewsRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.*;
import static java.lang.Long.parseLong;

@Service
public class NewsService {

    private final NewsRepository newsRepository;

    public NewsService(NewsRepository newsRepository) {
        this.newsRepository = newsRepository;
    }

    private final int PREVIEW_SIZE = 200;
    private NewsDTOcomparator comp = new NewsDTOcomparator();

    @Transactional(readOnly = true)
    public List<NewsDTO> getAllNews() {
        List<NewsDTO> results = new ArrayList<>();
        List<News> news = newsRepository.findAll();
        news.forEach((n) -> {
            NewsDTO dto = new NewsDTO(n.getId(), n.getDate(), n.getHeader(), n.getBody());
            results.add(dto);
        });
        Collections.sort(results, comp);
        return results;
    }

    @Transactional(readOnly = true)
    public List<NewsDTO> createNewsPreviews() {
        List<NewsDTO> results = new ArrayList<>();
        List<News> news = newsRepository.findAll();

        news.forEach((n) -> {
            String s = n.getBody();
            if (n.getBody().length() > PREVIEW_SIZE) {
                s = n.getBody().substring(0, PREVIEW_SIZE) + "...";
            }
            NewsDTO dto = new NewsDTO(n.getId(), n.getDate(), n.getHeader(), s);
            results.add(dto);
        });
        Collections.sort(results, comp);
        return results;
    }

    @Transactional
    public NewsDTO getNewsByID(String id) {
        News news = newsRepository.findById(parseLong(id)).get();
        return news.toDTO();
    }

    @Transactional
    public void deleteNews(List<Long> ids) {
        ids.forEach(id -> {
            Optional<News> news = newsRepository.findById(id);
            news.ifPresent(u -> {
                newsRepository.deleteById(u.getId());
            });
        });
    }

    @Transactional
    public void addNews(String header, String body) {
        Date date = new Date();
        News news = new News(date, header, body);
        newsRepository.save(news);
    }
}
