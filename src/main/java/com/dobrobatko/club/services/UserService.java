package com.dobrobatko.club.services;

import com.dobrobatko.club.configs.AppConfig;
import com.dobrobatko.club.dto.CustomUserDTO;
import com.dobrobatko.club.models.Country;
import com.dobrobatko.club.models.CustomUser;
import com.dobrobatko.club.models.Message;
import com.dobrobatko.club.models.UserRole;
import com.dobrobatko.club.repositories.UserRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.*;

@Service
public class UserService {
    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Transactional(readOnly = true)
    public CustomUser findByLogin(String login) {
        return userRepository.findByLogin(login);
    }

    @Transactional
    public void deleteUsers(List<Long> ids) {
        ids.forEach(id -> {
            Optional<CustomUser> user = userRepository.findById(id);
            user.ifPresent(u -> {
                if ( ! AppConfig.ADMIN.equals(u.getLogin())) {
                    userRepository.deleteById(u.getId());
                }
            });
        });
    }

    @Transactional
    public boolean addUser(String login, String passHash,
                           UserRole role, String email,
                           String bio) {
        if (userRepository.existsByLogin(login))
            return false;

        CustomUser user = new CustomUser(login, passHash, role, email, bio);
        Long adminId = userRepository.findByLogin("admin").getId();
        Message firstMsg = new Message(adminId, new Date(), "Welcome!", "Welcome to the Club!", user);
        user.addMessage(firstMsg);
        userRepository.save(user);
        return true;
    }

    @Transactional
    public void updateUser(Long id, String login, String email, String bio, Set<Country> countries) {
        CustomUser user = userRepository.findById(id).get();
        if (user == null)
            return;

        user.setEmail(email);
        user.setBio(bio);
        user.setCountries(countries);

        userRepository.save(user);
    }

    @Transactional(readOnly = true)
    public CustomUserDTO getUserById(Long id) {
        CustomUser user = userRepository.findById(id).get();
        return user.toDTO();
    }

    @Transactional(readOnly = true)
    public List<CustomUserDTO> getAllUsers() {
        List<CustomUserDTO> result = new ArrayList<>();
        List<CustomUser> users = userRepository.findAll();
        users.forEach(user -> {
            result.add(user.toDTO());
        });
        return result;
    }

    @Transactional
    public void addMessage (Long toUserId, Long fromUserId, String subject, String text) {
        CustomUser user = userRepository.findById(toUserId).get();
        if (user == null)
            return;
        Date timestamp = new Date();
        Message message = new Message(fromUserId, timestamp, subject, text, user);
        user.addMessage(message);
        userRepository.save(user);
    }

}
