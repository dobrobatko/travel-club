package com.dobrobatko.club.services;

import com.dobrobatko.club.dto.MessageDTO;
import com.dobrobatko.club.maintenance.MessageDTOcomparator;
import com.dobrobatko.club.models.CustomUser;
import com.dobrobatko.club.models.Message;
import com.dobrobatko.club.repositories.MessageRepository;
import com.dobrobatko.club.repositories.UserRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.*;

@Service
public class MessageService {
    private final MessageRepository messageRepository;
    private final UserRepository userRepository;

    public MessageService(MessageRepository messageRepository, UserRepository userRepository){
        this.messageRepository = messageRepository;
        this.userRepository = userRepository;
    }

    @Transactional (readOnly = true)
    public Integer howManyNew (Long id) {
        List<Message> messages;
        CustomUser user = userRepository.findById(id).get();
        Integer counter = user.getMsgCount();
        messages = user.getMessages();
        Integer newMsges = messages.size() - counter;
        return newMsges;
    }

    @Transactional (readOnly = true)
    public List<MessageDTO> getAllMessages (Long id) {
        CustomUser user = userRepository.findById(id).get();
        List<Message> messages = user.getMessages();
        List<MessageDTO> export = new ArrayList<>();
        for (Message msg : messages) {
            MessageDTO mdto = msg.toDTO();
            Long fromUserId = msg.getFromUserId();
            String fromUserName = "Unknown";
            try{
            fromUserName = userRepository.findById(fromUserId).get().getLogin();
            } catch (NoSuchElementException e) {
                e.printStackTrace();
                System.out.println("Error: Received message from deleted user");
            }
            mdto.setFromUserName(fromUserName);
            export.add(mdto);
        }
        MessageDTOcomparator comp = new MessageDTOcomparator();
        Collections.sort(export, comp);
        return export;
    }

    @Transactional
    public void resetCounter (Long id) {
        CustomUser user = userRepository.findById(id).get();
        List<Message> messages = user.getMessages();
        user.setMsgCount(messages.size());
        userRepository.save(user);
    }

    @Transactional
    public void deleteMessage (Long id) {
        messageRepository.deleteById(id);
    }

}
