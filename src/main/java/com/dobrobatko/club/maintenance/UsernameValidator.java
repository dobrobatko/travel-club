package com.dobrobatko.club.maintenance;

import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@NoArgsConstructor
public class UsernameValidator {

    private static final int USERNAME_LENGTH = 20;

    public boolean validate (String hex) {
        if(hex.length() > USERNAME_LENGTH) return false;
        return true;
    }
}
