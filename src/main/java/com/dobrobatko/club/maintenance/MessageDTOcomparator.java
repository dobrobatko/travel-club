package com.dobrobatko.club.maintenance;

import com.dobrobatko.club.dto.MessageDTO;
import java.util.Comparator;

public class MessageDTOcomparator implements Comparator<MessageDTO> {

    @Override
    public int compare(MessageDTO m1, MessageDTO m2) {
        if(m1.getId() > m2.getId()) {
            return -1;
        }
        if(m1.getId() < m2.getId()) {
            return 1;
        }
        return 0;
    }
}
