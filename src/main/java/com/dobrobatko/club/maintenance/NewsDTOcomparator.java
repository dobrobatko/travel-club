package com.dobrobatko.club.maintenance;

import com.dobrobatko.club.dto.NewsDTO;
import java.util.Comparator;

public class NewsDTOcomparator implements Comparator<NewsDTO> {

    @Override
    public int compare(NewsDTO m1, NewsDTO m2) {
        if(m1.getId() > m2.getId()) {
            return -1;
        }
        if(m1.getId() < m2.getId()) {
            return 1;
        }
        return 0;
    }
}
