package com.dobrobatko.club.repositories;

import com.dobrobatko.club.models.News;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface NewsRepository extends JpaRepository<News, Long> {

    @Override
    @Query("SELECT n FROM News n ORDER BY n.date DESC")
    List<News> findAll();
}
