package com.dobrobatko.club.repositories;

import com.dobrobatko.club.dto.CountryDTO;
import com.dobrobatko.club.models.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CountryRepository extends JpaRepository <Country, Long> {

    @Query("SELECT CASE WHEN COUNT(u) > 0 THEN true ELSE false END FROM Country u WHERE u.name = :name")
    boolean existsByName(@Param("name") String name);

    @Query("SELECT NEW com.dobrobatko.club.dto.CountryDTO(c.id, c.name) FROM Country c")
    List<CountryDTO> findAllNames();
}
