package com.dobrobatko.club.repositories;

import com.dobrobatko.club.models.Message;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MessageRepository extends JpaRepository<Message, Long> {
}
