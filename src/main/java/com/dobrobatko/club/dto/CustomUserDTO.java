package com.dobrobatko.club.dto;

import lombok.Data;
import java.util.Set;

@Data
public class CustomUserDTO {
    private Long id;
    private String login;
    private String email;
    private String bio;
    private Set<Long> countriesId;
    private Set<String> countriesName;

    private CustomUserDTO(Long id, String login, String email, String bio, Set<Long> countriesId, Set<String> countriesName) {
        this.id = id;
        this.login = login;
        this.email = email;
        this.bio = bio;
        this.countriesId = countriesId;
        this.countriesName = countriesName;
    }

    public CustomUserDTO(Long id, String login, String email, String bio) {
        this.id = id;
        this.login = login;
        this.email = email;
        this.bio = bio;
    }

    public static CustomUserDTO of(Long id, String login, String email, String bio, Set<Long> countriesId, Set<String> countriesName) {
        return new CustomUserDTO(id,login,email,bio,countriesId,countriesName);
    }

    public static CustomUserDTO of(Long id, String login, String email, String bio) {
        return new CustomUserDTO(id, login, email, bio);
    }

}
