package com.dobrobatko.club.dto;

import lombok.Data;
import java.util.Map;

@Data
public class CountryDTO {

    private Long id;
    private String name;
    private String description;
    private String imageLink;
    private Map<Long, String> users;


    public CountryDTO (Long id, String name) {
        this.id = id;
        this.name = name;
    }

    private CountryDTO(String name, String description, String imageLink) {
        this.name = name;
        this.description = description;
        this.imageLink = imageLink;
    }

    public CountryDTO(Long id, String name, String description, String imageLink, Map<Long, String> users) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.imageLink = imageLink;
        this.users = users;
    }

    private  CountryDTO(Long id, String name, String description, String imageLink) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.imageLink = imageLink;
    }

    public static CountryDTO of(String name, String description, String imageLink) {
        return new CountryDTO(name, description, imageLink);
    }

    public static CountryDTO of(Long id, String name, String description, String imageLink) {
        return new CountryDTO(id, name, description, imageLink);
    }

    public static CountryDTO of(Long id, String name, String description, String imageLink, Map<Long, String> users) {
        return new CountryDTO(id, name, description,imageLink, users);
    }

}
