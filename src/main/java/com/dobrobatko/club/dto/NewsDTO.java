package com.dobrobatko.club.dto;

import lombok.Data;
import java.util.Date;

@Data
public class NewsDTO {
    private Long id;
    private Date date;
    private String header;
    private String body;

    public NewsDTO(Long id, Date date, String header, String body) {
        this.id = id;
        this.date = date;
        this.header = header;
        this.body = body;
    }

    public NewsDTO(Date date, String header, String body) {
        this.date = date;
        this.header = header;
        this.body = body;
    }

    public static NewsDTO of(Long id, Date date, String header, String body) {
        return new NewsDTO(id, date,header, body);
    }

    public static NewsDTO of(Date date, String header, String body) {
        return new NewsDTO(date,header, body);
    }
}
