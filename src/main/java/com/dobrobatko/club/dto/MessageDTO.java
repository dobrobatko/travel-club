package com.dobrobatko.club.dto;

import lombok.Data;
import java.util.Date;

@Data
public class MessageDTO {
    private Long id;
    private Long fromUserId;
    private String fromUserName;
    private Date timestamp;
    private String subject;
    private String body;

    public MessageDTO(Long id, Long fromUserId, Date timestamp, String subject, String body) {
        this.id = id;
        this.fromUserId = fromUserId;
        this.timestamp = timestamp;
        this.subject = subject;
        this.body = body;
    }

    public MessageDTO(Long id, Long fromUserId, String fromUserName, Date timestamp, String subject, String body) {
        this.id = id;
        this.fromUserId = fromUserId;
        this.fromUserName = fromUserName;
        this.timestamp = timestamp;
        this.subject = subject;
        this.body = body;
    }

    public MessageDTO(Long fromUserId, Date timestamp, String subject, String body) {
        this.fromUserId = fromUserId;
        this.timestamp = timestamp;
        this.subject = subject;
        this.body = body;
    }

    public static MessageDTO of(Long fromUserId, Date timestamp, String subject, String body) {
        return new MessageDTO(fromUserId, timestamp, subject, body);
    }

    public static MessageDTO of(Long id, Long fromUserId, Date timestamp, String subject, String body) {
        return new MessageDTO(id, fromUserId, timestamp, subject, body);
    }



//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }
//
//    public Long getFromUserId() {
//        return fromUserId;
//    }
//
//    public void setFromUserId(Long fromUserId) {
//        this.fromUserId = fromUserId;
//    }
//
//    public Date getTimestamp() {
//        return timestamp;
//    }
//
//    public void setTimestamp(Date timestamp) {
//        this.timestamp = timestamp;
//    }
//
//    public String getSubject() {
//        return subject;
//    }
//
//    public void setSubject(String subject) {
//        this.subject = subject;
//    }
//
//    public String getBody() {
//        return body;
//    }
//
//    public void setBody(String body) {
//        this.body = body;
//    }
//
//    public String getFromUserName() {
//        return fromUserName;
//    }
//
//    public void setFromUserName(String fromUserName) {
//        this.fromUserName = fromUserName;
//    }
}
